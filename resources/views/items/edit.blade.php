<!-- app/views/nerds/edit.blade.php -->
@extends('layouts.app')
<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('items') }}">Item Alert</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('items') }}">View All Items</a></li>
            <li><a href="{{ URL::to('items/create') }}">Create a Item</a>
        </ul>
    </nav>

    <h1>Edit {{ $item->name }}</h1>

    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif


            <form method="post"  action="{{ action('ItemController@update',$item->id)  }}" enctype="multipart/form-data">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="PATCH">
            <div class="form-group">
                <label for="itemName">Item Name:</label>
                <input type="text" class="form-control" id="itemName" name="name" value="<?php echo $item->name?>">
            </div>
            <div class="form-group">
                <label for="price">Price:</label>
                <input type="text" class="form-control" id="price" name="price" value="<?php echo $item->price?>">
            </div>
            <div class="form-group">
                <label for="avatar">Upload an image:</label>
                <input type="file" name="avatar">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

</div>
</body>
</html>