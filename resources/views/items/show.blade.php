@extends('layouts.app')

@section('content')
        <div class="container">
                <table class="table table-striped">
                        <thead>
                        <tr>
                                <td>ID</td>
                                <td>Title</td>
                                <td>Price</td>
                                <td>Avatar</td>
                                <td colspan="2">Action</td>
                        </tr>
                        </thead>
                        <tbody>

                                <tr>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->price}}</td>
                                        <td>
                                                {{--{{$item->avatar}} --}}
                                                <img src="/{{ $item['avatar'] }}" height="100px" width="100px" /></td>
                                        <td><a href="{{action('ItemController@edit',$item->id)}}" class="btn btn-primary">Edit</a></td>
                                        <td>
                                                <form action="{{action('ItemController@destroy', $item->id)}}" method="post">
                                                        {{csrf_field()}}
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <button class="btn btn-danger" type="submit">Delete</button>
                                                </form>
                                        </td>
                                </tr>

                        </tbody>
                </table>
                </div>
@endsection