<!-- items.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Item Validation Form</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<div class="container">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif

    <form method="post" action="{{url('items')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="itemName">Item Name:</label>
            <input type="text" class="form-control" id="itemName" name="name">
        </div>
        <div class="form-group">
            <label for="price">Price:</label>
            <input type="text" class="form-control" id="price" name="price">
        </div>
        <div class="form-group">
            <label for="avatar">Upload an image:</label>
            <input type="file" name="avatar">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
</body>
</html>



<!-- app/views/nerds/create.blade.php -->

{{--<!DOCTYPE html>--}}
{{--<html>--}}
{{--<head>--}}
    {{--<title>Look! I'm CRUDding</title>--}}
    {{--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">--}}
{{--</head>--}}
{{--<body>--}}
{{--<div class="container">--}}

    {{--<nav class="navbar navbar-inverse">--}}
        {{--<div class="navbar-header">--}}
            {{--<a class="navbar-brand" href="{{ URL::to('items') }}">Nerd Alert</a>--}}
        {{--</div>--}}
        {{--<ul class="nav navbar-nav">--}}
            {{--<li><a href="{{ URL::to('items') }}">View All Items</a></li>--}}
            {{--<li><a href="{{ URL::to('items/create') }}">Create a Item</a>--}}
        {{--</ul>--}}
    {{--</nav>--}}

    {{--<h1>Create a Nerd</h1>--}}

    {{--<!-- if there are creation errors, they will show here -->--}}
    {{--{{ HTML::ul($errors->all()) }}--}}

    {{--{{ Form::open(array('url' => 'items')) }}--}}

    {{--<div class="form-group">--}}
        {{--{{ Form::label('name', 'Name') }}--}}
        {{--{{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
        {{--{{ Form::label('price', 'Price') }}--}}
        {{--{{ Form::text('price', Input::old('price'), array('class' => 'form-control')) }}--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
        {{--{{ Form::label('avatar', 'Avatar') }}--}}
        {{--{{ Form::file('avatar', Input::old('avatar'), array('class' => 'form-control')) }}--}}
    {{--</div>--}}

    {{--{{ Form::submit('Create the Item!', array('class' => 'btn btn-primary')) }}--}}

    {{--{{ Form::close() }}--}}

{{--</div>--}}
{{--</body>--}}
{{--</html>--}}
