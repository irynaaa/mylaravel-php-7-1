@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif

            <div class="form-group">
                <label for="title">Ticket id:</label>
                <p type="text" class="form-control">{{$ticket->id}} </p>
            </div>
                <div class="form-group">
                    <label for="title">Ticket Title:</label>
                    <p type="text" class="form-control">{{$ticket->title}} </p>
                </div>
                <div class="form-group">
                    <label for="description">Ticket Description:</label>
                    <p type="text" class="form-control">{{$ticket->description}}</p>
                </div>
            <a href="{{action('TicketController@index')}}" class="btn btn-primary">Back</a>
    </div>
@endsection