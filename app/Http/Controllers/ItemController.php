<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Item;
use App\Rules\FiveCharacters;
use Illuminate\Support\Facades\Redirect;

class ItemController extends Controller
{
    // ItemController.php

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \View::make('items.create');
       // return view('items');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the nerds
        $items = Item::all();

        // load the view and pass the nerds
        return \View::make('items.index')
            ->with('items', $items);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(ItemRequest $request)
    {

        $request->all();

        try{

            $item=new Item();

            $item->name=$request->get('name');
            $item->price=$request->get('price');
            $file = $request->file('avatar');

            $destinationPath = 'uploads';
            $file->move($destinationPath,$file->getClientOriginalName());
            $contents=$destinationPath.'/'.$file->getClientOriginalName();
            $item->avatar=$contents;

            $item->save();

            return Redirect::to('items');
        }
        catch (\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage());
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the item
        $item = Item::find($id);

        // show the view and pass the nerd to it
        return \View::make('items.show')
            ->with('item', $item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the item
        $item = Item::find($id);

        // show the edit form and pass the item
        return \View::make('items.edit')
            ->with('item', $item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ItemRequest $request, $id)
    {
        $request->all();
        try{
            $item=Item::find($id);
            $item->name=$request->get('name');
            $item->price=$request->get('price');
            $file = $request->file('avatar');

            $destinationPath = 'uploads';
            $file->move($destinationPath,$file->getClientOriginalName());
            $contents=$destinationPath.'/'.$file->getClientOriginalName();
            $item->avatar=$contents;

            $item->save();

            return Redirect::to('items');
        }
        catch (\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage());
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        $item->delete();

        return redirect('/items')->with('success', 'Item has been deleted!!');
    }
}
