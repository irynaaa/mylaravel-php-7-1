<?php
namespace App\Shop\Carousels\Repositories;
use App\Shop\Carousels\Carousel;
use App\Shop\Carousels\Exceptions\CreateCarouselErrorException;
use App\Shop\Carousels\Exceptions\UpdateCarouselErrorException;
use App\Shop\Carousels\Exceptions\DeleteCarouselErrorException;
use Illuminate\Database\QueryException;
class CarouselRepository
{
    /**
     * CarouselRepository constructor.
     * @param Carousel $carousel
     */
    public function __construct(Carousel $carousel)
    {
        $this->model = $carousel;
    }
    /**
     * @param array $data
     * @return Carousel
     * @throws CreateCarouselErrorException
     */
    public function createCarousel(array $data) : Carousel
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            throw new CreateCarouselErrorException($e);
        }
    }

    /**
     * @param int $id
     * @return Carousel
     * @throws CarouselNotFoundException
     */
    public function findCarousel(int $id) : Carousel
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new CarouselNotFoundException($e);
        }
    }

    /**
     * @param int $id
     * @return Carousel
     * @throws CarouselNotFoundException
     * * @throws UpdateCarouselErrorException
     */

    public function updateCarousel(int $id, array $data) : Carousel
    {
        try {
            $this->model->findOrFail($id);

        } catch (QueryException $e) {
            throw new UpdateCarouselErrorException($e);
        }

        try {
            $this->model->update($data);
            return $this->model;
        } catch (QueryException $e) {
            throw new UpdateCarouselErrorException($e);
        }

    }

    /**
     * @param int $id
     * @return Carousel
     * @throws CarouselNotFoundException
     * * @throws DeleteCarouselErrorException
     */
    public function deleteCarousel(int $id) : Carousel
    {
        try {
            $this->model->findOrFail($id);

        } catch (QueryException $e) {
            throw new UpdateCarouselErrorException($e);
        }

        try {
            $this->model->delete();
            return $this->model;
        } catch (QueryException $e) {
            throw new DeleteCarouselErrorException($e);
        }

    }

}
