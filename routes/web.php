<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/form',function(){
    return view('form');
});

Route::view('vue', 'vue');


Route::get('/user/{name?}',function($name = 'Virat Gandhi'){
    echo "Name: ".$name;
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users', 'HomeController@users');

Route::get('/create/ticket','TicketController@create');

Route::post('/create/ticket','TicketController@store');

Route::get('/tickets', 'TicketController@index');

Route::get('/edit/ticket/{id}','TicketController@edit');
Route::patch('/edit/ticket/{id}','TicketController@update');

Route::get('/show/ticket/{id}','TicketController@show');


Route::delete('/delete/ticket/{id}','TicketController@destroy');



// Second Route method – Root URL with ID will match this method
Route::get('ID/{id}',function($id){
    echo 'ID: '.$id;
});

// Third Route method – Root URL with or without name will match this method
Route::get('/user/{name?}',function($name = 'Virat Gandhi'){
    echo "Name: ".$name;
});


Route::get('localization/{locale}','LocalizationController@index');


Route::resource('items', 'ItemController');

//Route::get('items/index','ItemController@index');
//Route::get('items/create','ItemController@store');
////Route::get('items/store','ItemController@store');
//Route::post('/edit/item/{id}','ItemController@update');
//Route::patch('/edit/item/{id}','ItemController@update');
////Route::get('items/{id}', 'ItemController@show');


